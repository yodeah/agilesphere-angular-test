import { Weather } from './model/weather';

export interface AppState {
    weatherList : Array<Weather>
}
import { WeatherList } from './../../../model/weather';
import { getTempForTime } from './weather';

describe('WeatherSelector', () => {
    let weatherList:Array<WeatherList> = [
        {
            "dt": 1525294800,
            "main": {
                "temp": 279.29,
                "temp_min": 279.288,
                "temp_max": 279.29,
                "pressure": 1019.22,
                "sea_level": 1027.03,
                "grnd_level": 1019.22,
                "humidity": 93,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 0
            },
            "wind": {
                "speed": 2.81,
                "deg": 285.504
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2018-05-02 21:00:00"
        },
        {
            "dt": 1525305600,
            "main": {
                "temp": 277.51,
                "temp_min": 277.51,
                "temp_max": 277.513,
                "pressure": 1021.4,
                "sea_level": 1029.18,
                "grnd_level": 1021.4,
                "humidity": 96,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "clouds": {
                "all": 0
            },
            "wind": {
                "speed": 2.79,
                "deg": 269.005
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2018-05-03 00:00:00"
        },
        {
            "dt": 1525316400,
            "main": {
                "temp": 276.31,
                "temp_min": 276.305,
                "temp_max": 276.31,
                "pressure": 1022.73,
                "sea_level": 1030.56,
                "grnd_level": 1022.73,
                "humidity": 98,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "02n"
                }
            ],
            "clouds": {
                "all": 8
            },
            "wind": {
                "speed": 2.51,
                "deg": 264.01
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2018-05-03 03:00:00"
        }
    ];


    it('should select correct weather for given time', () => {
        expect(getTempForTime('21:00:00', weatherList)).toEqual(279.29);
      });
    
    });
import { WeatherDisplay, WeatherList } from './../../../model/weather';
import { Weather } from "../../../model/weather";

// Time should look like 06:00:00 //
export const getTempForTime = (time:string , weatherList:Array<WeatherList>):number => {
    return weatherList.find( (w:WeatherList) => w.dt_txt.split(" ")[1] === time ).main.temp;
};
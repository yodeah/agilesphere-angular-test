import { Weather } from './../../../model/weather';
import { WEATHER_ACTIONS } from '../actions/weather';

export function weatherReducer( state: Array<Weather> = [], action): Array<Weather> {
    switch (action.type) {
        case WEATHER_ACTIONS.WEATHER_LOADED:
            return [...state, action.payload];
        default:
            return state;
    }
}
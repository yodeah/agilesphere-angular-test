import { WeatherDisplay } from './../model/weather';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Weather } from '../model/weather';
import { WEATHER_ACTIONS } from './store/actions/weather';
import { WeatherStoreService } from './services/weather.store.service';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-weather',
  template: `
  <app-search (city)="citySearch($event)"></app-search>
  <app-results [weatherList]="weatherDisplayElements"></app-results>  `
})
export class WeatherContainer implements OnInit{
 
  public weatherDisplayElements:Observable<Array<WeatherDisplay>>;

  constructor(
    private weatherStoreService:WeatherStoreService,
    private weatherService:WeatherService
  ) { }

  ngOnInit(): void {
    this.weatherDisplayElements = this.weatherStoreService.getWeatherDisplayElements();
  }

  private successfulSearch = (weather:Weather) => {
    this.weatherStoreService.weatherLoaded(weather);
  };

  private failedSearch = (e:HttpErrorResponse) => {
    if(e.status == 404){
      alert("This city doesnt exist!");
    }else{
      alert("Contact admin!");
    }
  };

  public citySearch(city:string) {
    this.weatherService.searchWeatherForCity(city)
    .then(this.successfulSearch)
    .catch(this.failedSearch);
  }
}

import { Injectable } from "@angular/core";
import { Store } from '@ngrx/store';
import { Observable } from "rxjs/Observable";
// import { map } from 'rxjs/operators';
import 'rxjs/Rx'; // sadly had to import this to make map work increases budle size

import { AppState } from "../../app.state";
import { Weather, WeatherDisplay } from "../../model/weather";
import { getTempForTime } from "../store/selectors/weather";
import { WEATHER_ACTIONS } from "../store/actions/weather";


@Injectable()
export class WeatherStoreService {
    constructor(private store: Store<AppState>) { }

    private mapWeatherObjectToWeatherDisplay = (weather:Weather):WeatherDisplay => {
        return { 
            "city" : weather.city.name,
            "temp_6am": getTempForTime("06:00:00",weather.list),
            "temp_12pm": getTempForTime("12:00:00",weather.list),
            "temp_6pm": getTempForTime("18:00:00",weather.list),
            "temp_12am": getTempForTime("00:00:00",weather.list)
        };
    }

    public weatherLoaded(weather: Weather) {
        this.store.dispatch({
            type: WEATHER_ACTIONS.WEATHER_LOADED,
            payload: weather
        });
    }

    public getWeatherDisplayElements():Observable<Array<WeatherDisplay>> {
        return this.store.select('weatherList')
        .map( (weatherList:Array<Weather>):Array<WeatherDisplay> => {
            return weatherList.map(this.mapWeatherObjectToWeatherDisplay);
        });
    }

}
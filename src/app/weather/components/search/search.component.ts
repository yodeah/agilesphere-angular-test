import { Component, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';

import { Weather } from './../../../model/weather';
import { WEATHER_ACTIONS } from '../../store/actions/weather';
import { AppState } from '../../../app.state';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {
  @Output() city: EventEmitter<string> = new EventEmitter();

  cityInput: string;

  search() { this.city.emit(this.cityInput); } 
}

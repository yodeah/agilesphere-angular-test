import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component, OnChanges, Input } from '@angular/core';

import { AppState } from '../../../app.state';
import { Weather, WeatherList, WeatherDisplay } from './../../../model/weather';
import { WeatherStoreService } from '../../services/weather.store.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html'
})
export class ResultsComponent implements OnChanges {

  @Input()
  public weatherList:Observable<Array<WeatherDisplay>>;

  ngOnChanges() {
    // IMPLEMENT ANYTHING YOU BEKIEVE YOU MIGHT NEED HERE
  }
}



import { Observable } from 'rxjs/Observable';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ResultsComponent } from './results.component';
import { WeatherStoreService } from '../../services/weather.store.service';
import { WeatherDisplay } from '../../../model/weather';


    export const weatherDisplay:Array<WeatherDisplay> = [
        {
            'city': 'Shanghai',
            'temp_6am': 11,
            'temp_12pm': 20,
            'temp_6pm': 16,
            'temp_12am': 9
        },
        {
            'city': 'Budapest',
            'temp_6am': 16,
            'temp_12pm': 29,
            'temp_6pm': 19,
            'temp_12am': 14
        }
    ];


describe('ResultsComponent', () => {
  let component: ResultsComponent;
  let fixture: ComponentFixture<ResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsComponent ],
      imports: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display data from input', () => {
    fixture.componentInstance.weatherList = Observable.of(weatherDisplay);
    fixture.detectChanges();
    let tableData = fixture.debugElement.nativeElement.querySelectorAll('tr td');
    expect(tableData[0].innerText).toBe('Shanghai');
    expect(tableData[1].innerText).toBe('11 °C');
    expect(tableData[5].innerText).toBe('Budapest');
    expect(tableData[6].innerText).toBe('16 °C');
  });

});

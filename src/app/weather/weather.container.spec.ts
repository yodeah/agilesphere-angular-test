import { Observable } from 'rxjs/Observable';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Component, Injectable } from '@angular/core';

import { WeatherContainer } from './weather.container';
import { Weather, WeatherDisplay } from '../model/weather';
import { WeatherService } from './services/weather.service';
import { WeatherStoreService } from './services/weather.store.service';
import { weatherDisplay } from './components/results/results.component.spec';



class MockWeatherService {
  searchWeatherForCity(city):Promise<Weather> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({"cod":"hello"});
      }, 1);
    });
  }

  
}

class MockWeatherStoreService{
  public weatherLoaded(weather: Weather){}

  public getWeatherDisplayElements():Observable<Array<WeatherDisplay>> {
    return Observable.of(weatherDisplay);
  }
}

describe('WeatherContainer', () => {
  let component: WeatherContainer;
  let fixture: ComponentFixture<WeatherContainer>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherContainer ],
      imports: [],
      providers: [
        {provide: WeatherService, useClass: MockWeatherService},
        {provide: WeatherStoreService, useClass: MockWeatherStoreService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call WeatherStoreService with the requested data from WeatherService when citySearch called', fakeAsync(() => {
    const weatherStoreService = fixture.debugElement.injector.get(WeatherStoreService);
    spyOn(weatherStoreService, 'weatherLoaded');

    component.citySearch("Shanghai");

    tick(2); 
    fixture.detectChanges();

    expect(weatherStoreService.weatherLoaded).toHaveBeenCalledWith({"cod":"hello"});
  }));

  ///We could also check an error message here if the request failed


});
